# Queue class using a Singular LinkedList
class LinkedQueue
  
  class LinkedNode
    constructor: (@content, @next) ->

  head: null
  tail: null
  size: 0

  enqueue: (item) ->
    if @head?
      @tail.next = new LinkedNode(item, null)
      @tail = @tail.next
    else
      @head = new LinkedNode(item, null)
      @tail = @head
    @size++

  dequeue: ->
    v = @head.content
    @head = @head.next
    @size--
    v

  empty: ->
    @size == 0

# A single cell state in the grid
class MinesweeperCell
  mined: false
  uncovered: false
  inqueue: false
  minedneighbours: 0
  flagged: false

  constructor: (x,y,size) ->
    @left = x*size+x
    @top = y*size+y
    @xi = x
    @yi = y
    @size = size

# Minesweeper game
class Minesweeper
  cellSize: 20
  tickLength: 20
  canvas: null
  g: null
  cells: []
  mines: []
  flagged: 0
  uncovered: 0
  fillqueue: new LinkedQueue
  running: true
  winOrLose: null
  canvasOffsetX: 0
  canvasOffsetY: 0
  redrawRequired: true

  constructor: (rows, columns, numberOfMines) ->
    # vars
    @rows = rows
    @columns = columns
    @numberOfMines = numberOfMines
    @firstmove = true

    # initialise game
    @createCanvas()
    @resizeCanvas()
    @createDrawingContext()
    @createGameGrid()

    # ovveride context menu
    @canvas.addEventListener("contextmenu", @onContextMenu, false)

    # mouse click on cell
    @canvas.addEventListener("mousedown", @onMouseDown, false)

    # start loop
    @tick()

  createCanvas: ->
    @canvas = document.createElement 'canvas'
    document.body.appendChild @canvas

  resizeCanvas: ->
    @canvas.height = @cellSize * @rows + @rows
    @canvas.width = @cellSize * @columns + @columns + 200

    @canvasOffsetX = @canvas.offsetLeft
    @canvasOffsetY = @canvas.offsetTop

  createDrawingContext: ->
    @g = @canvas.getContext '2d'
    @g.font = "18px Arial"
    @g.textBaseline = "middle"
    @g.textAlign = "center"

  createGameGrid: ->
    # Create grid of cells
    @cells = []
    for y in [0..@rows-1]
      @cells.push []
      for x in [0..@columns-1]
        @cells[y].push new MinesweeperCell x, y, @cellSize
    true

  spawnMines: (safeposX, safeposY) ->
    @mines = []
    # Spawn mines in the grid
    while @mines.length < @numberOfMines
      # Create random position
      rx = Math.floor(Math.random() * @columns)
      ry = Math.floor(Math.random() * @rows)
      if rx != safeposX or ry != safeposY
        mc = @cells[ry][rx]
        if not mc.mined
          mc.mined = true
          @mines.push mc

          # Increase number of mines next to neighbours
          for y in [-1..1]
            for x in [-1..1]
              dx = rx+x
              dy = ry+y
              if 0 <= dx < @columns and 0 <= dy < @rows  
                mc = @cells[dy][dx]
                mc.minedneighbours++
    return true

  onContextMenu:(e) -> 
    e.preventDefault()
    return false

  onMouseDown: (e) =>
    # Only allow clicks if running
    if @running
      # calculate x, y block coordinate
      xi = Math.floor ((e.pageX-@canvasOffsetX) / (@cellSize+1))
      yi = Math.floor ((e.pageY-@canvasOffsetY) / (@cellSize+1))

      # only accept in range
      if 0 <= xi < @columns and 0 <= yi < @rows   
        # Left Click
        if e.which == 1
          # if this is the first click.. mines need to be spawned
          if @firstmove
            @spawnMines xi, yi
            @firstmove = false

          mc = @cells[yi][xi]
          if not mc.mined
            @fill mc
          else
            for m in @mines
              m.uncovered = true
            @running = false
            @winOrLose = 'lose' 

        # Right Click
        else if e.which == 3
          mc = @cells[yi][xi]
          if not mc.uncovered
            if mc.flagged
              mc.flagged = false
              @flagged--
            else 
              mc.flagged = true
              @flagged++

    @redrawRequired = true
    return true

  drawCell: (x,y) ->
    mc = @cells[y][x]
    if mc.uncovered
      if mc.mined
        @g.fillStyle = "rgb(200,0,0)"
        @g.fillRect mc.left, mc.top, @cellSize, @cellSize 
      else
        @g.fillStyle = "rgb(200,200,200)"
        @g.fillRect mc.left, mc.top, @cellSize, @cellSize 
        if mc.minedneighbours > 0
          @g.fillStyle = 'rgb(0,0,0)'
          @g.fillText "" + mc.minedneighbours, mc.left+@cellSize/2, mc.top + @cellSize/2 + 1
    else
      if mc.flagged
        @g.fillStyle = "rgb(0,100,0)"
      else
        @g.fillStyle = "rgb(100,100,100)"
      @g.fillRect mc.left, mc.top, @cellSize, @cellSize 
    return true

  drawGameGrid: ->
    for y in [0..@rows-1]
      for x in [0..@columns-1]
        @drawCell(x,y)
    return true

  fill: (cell) ->
    cell.uncovered = true
    @uncovered++

    if cell.minedneighbours == 0
      for y in [-1..1]
        for x in [-1..1]
          dx = cell.xi+x
          dy = cell.yi+y
          if 0 <= dx < @columns and 0 <= dy < @rows
            mc = @cells[dy][dx]
            if not mc.mined and not mc.inqueue and not mc.uncovered
              mc.inqueue = true
              @fillqueue.enqueue mc

    if @fillqueue.empty()
      if @uncovered == @rows * @columns - @numberOfMines
        @running = false
        @winOrLose = 'win'

    return true

  tick: =>
    for i in [0..20]
      if not @fillqueue.empty()
        @fill @fillqueue.dequeue()
        @redrawRequired = true

    if @redrawRequired
      @g.fillStyle = "rgb(20,20,20)"
      @g.fillRect 0, 0, @canvas.width, @canvas.height 
      
      @drawGameGrid()

      @g.fillStyle = "rgb(200,255,255)"
      @g.fillText "Flags: #{@flagged}/#{@numberOfMines}", @columns * @cellSize + 100, 40

      if not @running
        @g.shadowBlur = 30
        @g.shadowColor = "black"
        @g.fillStyle = "rgb(20,20,20)"
        @g.fillRect @canvas.width/2-100, @canvas.height/2 - 20, 200, 40

        @g.fillStyle = "rgb(40,40,40)"
        @g.fillRect @canvas.width/2-100+3, @canvas.height/2 - 20+3, 200-6, 40-6

        @g.fillStyle = "rgb(200,255,255)"
        if @winOrLose == 'win'
          @g.fillText "You Won!    :D", @canvas.width/2, @canvas.height/2
        else
          @g.fillText "You Lost    :(", @canvas.width/2, @canvas.height/2
      @redrawRequired = false

    setTimeout @tick, @tickLength
    return true

window.Minesweeper = Minesweeper